<?php

namespace App\Controller;

use App\Form\SimulatorType;
use App\Services\SimulatorManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class DefaultController extends AbstractController
{
    /** @var Security $security */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/simulator", name="simulator")
     */
    public function index(Request $request, SimulatorManager $simulatorManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $form = $this->createForm(SimulatorType::class);
        $form->handleRequest($request);
        $echeancies = null;
        if ($form->isSubmitted()) {
            $echeancies = $simulatorManager->calculEcheancies($form->get('montant')->getData(), $form->get('taux')->getData(), $form->get('nbMonths')->getData());
        }
        return $this->render('simulator/index.html.twig', [
            'form' => $form->createView(),
            'echancies' => $echeancies
        ]);
    }
}