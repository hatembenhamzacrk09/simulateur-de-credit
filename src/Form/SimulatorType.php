<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;


class SimulatorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('montant',NumberType::class,[
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Assert\Type(['type' => 'float'])
                ]
            ])
            ->add('taux',NumberType::class,[
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Assert\Type(['type' => 'float'])
                ]
            ])
            ->add('nbMonths',IntegerType::class,[
                'required' => true,
                'label' => 'Nombre des mois',
                'constraints' => [
                    new NotBlank(),
                    new Assert\Type(['type' => 'integer'])
                ]
            ])
        ;
    }
}
