<?php

namespace App\Services;

class SimulatorManager
{
    public function calculEcheancies(float $montant, float $taux, int $nbMonths): ?string
    {
        $montant += $montant * $taux * 0.01;
        return number_format(($montant / $nbMonths), 2);
    }
}